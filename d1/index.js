// Array methods

// Mutator methods

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push()
// adds element to tail

console.log('Current Array: ');
console.log(fruits);
let fruitsLength = fruits.push('Mango'); // 5
console.log(fruitsLength); // 5
console.log(fruits);

fruits.push('Avocado', 'Guava');

// pop()
// removes and returns last element

let removedFruit = fruits.pop()
console.log(removedFruit);
console.log(fruits);

// activity

let ghostFighters = ['Eugene', 'Dennis', 'Alfred', 'Taguro'];

function unfriendGF() {
  ghostFighters.pop();
}

unfriendGF();
console.log(ghostFighters);

// unshift()
// Adds one or more elements at the beginning of an erray

fruits.unshift('Line', 'Banana');
console.log('Unshif test:');
console.log(fruits);

// shift()
// removes and returns element at the start

// splice()
// simultaneously removes and adds elements in a specified index number
// arr.splice(startIndex, deleteCount, elementsToBeAdded)

fruits.splice(1, 2, 'Lime', 'Cherry', 'Fruit1')

console.log('After splice');
console.log(fruits);

// sort()
// Sorts in alphanumeric order

// reverse()

// Non-Mutator methods

let countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];


// indexOf()
// returns index of first matching element
// if no match, return -1

let firstIndex = countries.indexOf('PH', 3 );
console.log('firstIndex: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('invalidCountry: ' + invalidCountry);

// lastIndexOf()
// returns index of last matching element or -1


// slice()
// slice(startIndex)
// slice(startIndex, endIndex)

// toString()

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

// concat()
let tasks = taskArrayA.concat(taskArrayB);
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)

// combine multiple arrays
console.log('Result of concat method');

// join()
// returns array as a string separated by a specified separateor

let users = ['John', 'Jane', 'Joe', 'Robert', 'Nej'];
console.log(users.join());

console.log(users.join(''));
console.log(users.join(' - '));

// Iteration methods

// forEach()

console.log('foEach activity');
ghostFighters.forEach(function (gf) {
  console.log(gf);
});

// Using foreach with conditional statements
let filteredTasks = [];

allTasks.forEach(function (task) {
  console.log(task);
  if (task.length > 10) {
    filteredTasks.push(task);
  }
})

console.log(filteredTasks);

// map()
let numbers = [1, 2, 3, 4, 5];

let numberMap  = numbers.map(function (number) {
  return number * number;
});
console.log(numberMap);

// map() vs forEach()
let numberForEach = numbers.forEach(function (number) {
  return number * number;
});

console.log(numberForEach);

// every()
// returns true if all elements meet condition

let allValid = numbers.every(function (number) {
  return (number < 3);
});

console.log(allValid); // f

// some()
// returns true if at least once element meets condition

let someValid = numbers.some(function (number) {
  return (number < 2);
});

console.log('somevalid: ' + someValid);

if (someValid) {
  console.log('Some numbers in the array are greater than 2');
}

// filter()

let filterValid = numbers.filter(function (number) {
  return (number < 3);
});

console.log(filterValid); // [1, 2]

let nothingFound = numbers.filter(function (number) {
  return (number === 0);
});

console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = [];
numbers.forEach(function (number) {
  console.log(number);

  if (number < 3) {
    filteredNumbers.push(number);
  }
});

console.log(filteredNumbers);

// includes()

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound = products.includes("Mouse");
console.log(productFound);

let productNotFound = products.includes('Headset');
console.log(productNotFound);

// Method chaining

// Mini activity
// addTrainer
// receive string
// determine if already exists in contacts array
// if y, show alert "Already added in the Match Call"
// if n, add trainer and show alert "Registered!"
// invoke and add a trainer in the browser's console
// log the contacts array

let contacts = ['Ash']

function addTrainer(trainer) {
  if (contacts.map(t => t.toLowerCase())
              .includes(trainer.toLowerCase())
  ){
    //alert('Already added in the Match Call');
  } else {
    contacts.push(trainer);
    //alert('Registered!');
  }
}

console.log(`contacts: ${contacts}`);
console.log(`calling addTrainer('Ash')`);
addTrainer('Ash');
console.log(`contacts: ${contacts}`);
console.log(`calling addTrainer('ash')`);
addTrainer('ash');
console.log(`contacts: ${contacts}`);
console.log(`calling addTrainer('Misty')`);
addTrainer('Misty');
console.log(`contacts: ${contacts}`);

// reduce
/* evaluates array from LTR, and returns the reduced value

  arr.reduce(function (accumulator, currVal) {
    return expression/operation
  }
  accumulator - stores the result of every iter
  currVal - current/next element in the array that is evaluated in each iter
*/

console.log(numbers);
let iteration = 0;
let iterationStr = 0;

let reducedArray = numbers.reduce(function (x, y) {
  console.warn('current iteration: ' + ++iteration);
  console.log('accumulator: ' + x);
  console.log('current value: ' + y);
  return x + y;
});

console.log(`Result of reduce: ${reducedArray}`);


let list = ['Hello', 'Again', 'World'];
let reducedJoin = list.reduce(function (x, y) {
  console.warn('current iteration: ' + ++iteration);
  console.log('accumulator: ' + x);
  console.log('current value: ' + y);
  return x + ' ' + y;
});

console.log(`Result of reduce: ${reducedJoin}`);
